import os

from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QDialog, QGridLayout, QLabel, QComboBox, QTabWidget, QVBoxLayout, QWidget, QHBoxLayout, \
    QCheckBox, QDoubleSpinBox, QPushButton
from qt_material import apply_stylesheet, QtStyleTools


class ParametresDialog(QDialog):
    def __init__(self, delegue_master):
        super().__init__()
        self.delegue_master = delegue_master

        # Module de traduction pour les langues
        tr = self.delegue_master.langues.tr

        # Définition de la taille de la fenêtre
        self.setGeometry(100, 100, 400, 200)

        # Centrer la fenêtre sur l'écran
        self.move(self.screen().geometry().center() - self.rect().center())

        # Layout principal de la fenêtre
        layout_principal = QVBoxLayout(self)

        # Onglets de paramètres
        ParametresDialog.onglets_parametre = QTabWidget()

        # Onglet pour les paramètres généraux
        onglet_general = QWidget()
        onglet_general_layout = QGridLayout()
        onglet_general.setLayout(onglet_general_layout)

        # Label pour la note maximale
        ParametresDialog.label_note_max = QLabel(tr('note_max') + ":")
        onglet_general_layout.addWidget(ParametresDialog.label_note_max, 0, 0)

        # Zone de saisie pour la note maximale
        note_maximum = QDoubleSpinBox()
        note_maximum.setValue(self.delegue_master.profile.note_max)
        note_maximum.valueChanged.connect(lambda value: self.changer_note_maximum(value))
        onglet_general_layout.addWidget(note_maximum, 0, 1)

        # Bouton pour nettoyer le cache
        ParametresDialog.label_nettoyer_cache = QLabel(tr('nettoyer_cache') + ": (" + self.taille_cache() + ")")
        onglet_general_layout.addWidget(ParametresDialog.label_nettoyer_cache, 1, 0)

        nettoyer_cache = QPushButton()
        nettoyer_cache.setText(tr('nettoyer_cache_bouton'))
        nettoyer_cache.clicked.connect(lambda: self.nettoyer_cache())
        onglet_general_layout.addWidget(nettoyer_cache, 1, 1)

        # Onglet pour les langues
        onglet_langues = QWidget()
        onglet_langues_layout = QGridLayout()
        onglet_langues.setLayout(onglet_langues_layout)

        langue_combo = QComboBox() # ComboBox pour les langues
        langue_combo.addItems(self.delegue_master.langues.langues_noms)
        index = self.delegue_master.langues.langues.index(self.delegue_master.profile.langue)
        langue_combo.setCurrentIndex(index)
        langue_combo.currentIndexChanged.connect(lambda index: self.changer_langue(index))
        onglet_langues_layout.addWidget(langue_combo)

        # Onglet pour les thèmes
        onglet_themes = QWidget()
        onglet_themes_layout = QHBoxLayout()
        onglet_themes.setLayout(onglet_themes_layout)

        self.themes_combo = QComboBox()
        self.bouton_sombre = QCheckBox(tr('sombre'))
        self.bouton_sombre.setChecked(self.delegue_master.profile.dark)

        self.light_colors = self.delegue_master.themes.light_color()
        self.dark_colors = self.delegue_master.themes.dark_color()

        if self.bouton_sombre.isChecked(): # Sombre
            self.themes_combo.addItems(self.dark_colors)
        else:
            self.themes_combo.addItems(self.light_colors)

        self.themes_combo.setCurrentIndex(self.delegue_master.profile.theme)

        self.bouton_sombre.toggled.connect(lambda checked: self.change_clair_sombre(self.themes_combo.currentIndex()))
        self.themes_combo.currentIndexChanged.connect(
            lambda index: self.changer_theme(self.themes_combo.currentIndex()))

        self.bouton_personalisation = QPushButton("+")
        self.bouton_personalisation.clicked.connect(lambda: self.personalisation())


        onglet_themes_layout.addWidget(self.themes_combo)
        onglet_themes_layout.addWidget(self.bouton_sombre)
        #onglet_themes_layout.addWidget(self.bouton_personalisation)

        # Ajout des onglets à la fenêtre principale
        ParametresDialog.onglets_parametre.addTab(onglet_general, tr('generale'))
        ParametresDialog.onglets_parametre.addTab(onglet_langues, tr('langues'))
        ParametresDialog.onglets_parametre.addTab(onglet_themes, tr('themes'))

        layout_principal.addWidget(ParametresDialog.onglets_parametre)

        # Configuration du titre et de l'icône de la fenêtre
        self.setWindowTitle(tr('parametres'))
        self.setWindowIcon(QIcon("logo.png"))

        # Application du thème sur la fenêtre ParametresDialog
        apply_stylesheet(self, theme=self.delegue_master.themes.convertir_theme_int_en_str())

    # Méthode appelée lorsqu'une modification de la note maximale est effectuée
    def changer_note_maximum(self, value):
        # Mise à jour de la note maximale dans le profil
        self.delegue_master.profile.note_max = value
        # Enregistrement du profil mis à jour
        self.delegue_master.profile.enregistrer_fichier()
        # Appel d'une méthode pour mettre à jour l'affichage des conseils
        self.delegue_master.recup_onglet_conseils().changer_note_maximum()

    # Méthode pour nettoyer le cache
    def nettoyer_cache(self):
        try:
            # On supprime tous les fichiers du dossier temporaire
            temp_dir = self.delegue_master.temp_dir
            for filename in os.listdir(temp_dir):
                file_path = os.path.join(temp_dir, filename)
                if os.path.isfile(file_path) and not os.path.samefile(file_path, self.delegue_master.db_temp_file):
                    os.unlink(file_path)
        except Exception as e:
            # En cas d'erreur, affichage d'un message d'erreur
            print('Impossible de supprimer le fichier %s. Reason: %s' % (file_path, e))

        # Mise à jour de l'étiquette pour afficher la taille du cache
        ParametresDialog.label_nettoyer_cache.setText(
            self.delegue_master.langues.tr('nettoyer_cache') + ": (" + self.taille_cache() + ")")

    # Méthode pour obtenir la taille du cache
    def taille_cache(self):
        temp_dir = self.delegue_master.temp_dir
        # Calcul de la taille totale du cache en Mo
        return "{:.2f} Mo".format(
            sum(os.path.getsize(os.path.join(temp_dir, f)) for f in os.listdir(temp_dir)) / 1024 / 1024)

    # Méthode appelée lorsqu'une langue est sélectionnée
    def changer_langue(self, index):
        # Changement de la langue dans le profil
        langue_actuelle = self.delegue_master.langues.changer_langue(index)
        self.delegue_master.profile.langue = langue_actuelle
        # Enregistrement du profil mis à jour
        self.delegue_master.profile.enregistrer_fichier()

        # Mise à jour des textes dans les onglets avec la nouvelle langue
        tr = self.delegue_master.langues.tr
        ParametresDialog.onglets_parametre.setTabText(0, tr('generale'))
        ParametresDialog.onglets_parametre.setTabText(1, tr('langues'))
        ParametresDialog.onglets_parametre.setTabText(2, tr('themes'))

        self.bouton_sombre.setText(tr('sombre'))
        ParametresDialog.label_note_max.setText(tr('note_max') + ":")

        self.setWindowTitle(tr('parametres'))

    # Méthode appelée lorsqu'on bascule entre le thème clair et sombre
    def change_clair_sombre(self, index):
        # Récupération du texte du thème actuel
        ancien_text = self.themes_combo.currentText().lower().replace("dark ", "")

        self.themes_combo.clear()
        # Ajout des couleurs du thème sombre si le mode sombre est activé
        if self.bouton_sombre.isChecked():
            self.delegue_master.profile.dark = True
            self.themes_combo.addItems(self.dark_colors)
            for color in self.dark_colors:
                if ancien_text in color.lower():
                    i = self.dark_colors.index(color)
                    self.themes_combo.setCurrentIndex(i)
                    break

            self.themes_combo.setCurrentIndex(index)
        # Ajout des couleurs du thème clair si le mode sombre est désactivé
        else:
            self.delegue_master.profile.dark = False
            self.themes_combo.addItems(self.light_colors)
            for color in self.light_colors:
                if ancien_text in color.lower():
                    i = self.dark_colors.index(color)
                    self.themes_combo.setCurrentIndex(i)
                    break

        # Enregistrement du profil mis à jour
        self.delegue_master.profile.enregistrer_fichier()

    # Méthode pour changer le thème de l'application
    def changer_theme(self, index):
        # Mise à jour du thème dans le profil
        self.delegue_master.profile.theme = index
        # Enregistrement du profil mis à jour
        self.delegue_master.profile.enregistrer_fichier()

        # Application du nouveau thème à l'application
        apply_stylesheet(self.delegue_master, theme=self.delegue_master.themes.convertir_theme_int_en_str())
        apply_stylesheet(self, theme=self.delegue_master.themes.convertir_theme_int_en_str())

    def personalisation(self):
        style = QtStyleTools()
        style.show_dock_theme(self.delegue_master)
