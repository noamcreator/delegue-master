# Importation des modules nécessaires
import os
import shutil
import sys
import tempfile

# Importations PyQt6 pour l'interface graphique
from PyQt6.QtGui import QIcon
from PyQt6.QtWidgets import QMainWindow, QTabWidget, QWidget, QVBoxLayout, QApplication

# Importation de la barre de menu personnalisée
from Menu import MenuBar

# Importation des onglets
from onglets.conseils import OngletConseils
from onglets.eleves import OngletEleves

# Importation du module pour le thème
import qt_material

# Importation des classes de paramètres
from sources.code.parametres.Langues import Langues
from sources.code.parametres.Profile import Profile
from sources.code.parametres.Themes import Themes


# Classe principale de Délegué Master
class DelegueMaster(QMainWindow):
    def __init__(self):
        super().__init__()

        # Initialisation des classes de paramètres
        self.profile = Profile()
        self.langues = Langues(self)
        self.themes = Themes(self)

        # Création d'un dossier temporaire pour la base de données
        self.temp_dir = os.path.join(tempfile.gettempdir(), 'délégué_master')
        if not os.path.exists(self.temp_dir):
            os.makedirs(self.temp_dir)

        # Création d'un fichier de base de données temporaire à partir d'un modèle
        _, self.db_temp_file = tempfile.mkstemp(dir=self.temp_dir, suffix='.db')
        script_dir = os.path.dirname(os.path.abspath(__file__).replace("code", ""))
        model_file = os.path.join(script_dir, 'ressources\\', 'eleves_vide.db')
        shutil.copy(model_file, self.db_temp_file)

        # Configuration de la fenêtre principale
        self.setGeometry(100, 100, 800, 600)
        self.move(self.screen().geometry().center() - self.rect().center())

        # Création d'un widget onglets et définition comme widget central
        self.onglets_widget = QTabWidget()
        self.widget_central = QWidget()
        self.setCentralWidget(self.widget_central)
        self.layout_central = QVBoxLayout()
        self.layout_central.addWidget(self.onglets_widget)
        self.widget_central.setLayout(self.layout_central)

        # Ajout des onglets à l'onglet widget
        eleve_tab = OngletEleves(self)
        self.onglets_widget.addTab(eleve_tab, self.langues.tr('eleves'))
        conseil_tab = OngletConseils(self)
        self.onglets_widget.addTab(conseil_tab, self.langues.tr('conseils'))

        # Connexion du signal de changement d'onglet à la fonction de rappel
        self.onglets_widget.currentChanged.connect(self.quand_table_change)

        # Création et configuration de la barre de menu
        self.menuBar = MenuBar(self)

        # Configuration de la fenêtre principale
        self.setWindowTitle(self.langues.tr('delegue_master'))
        self.setWindowIcon(QIcon("logo.png"))

    # Méthode pour récupérer l'onglet des élèves
    def recup_onglet_eleves(self):
        return self.onglets_widget.widget(0)

    # Méthode pour récupérer l'onglet des conseils
    def recup_onglet_conseils(self):
        return self.onglets_widget.widget(1)

    # Méthode appelée lors du changement d'onglet
    def quand_table_change(self, index):
        if index == 1:  # Si l'onglet "Conseil de Classe" est sélectionné
            # Permettre le changement de la combobox dans l'onglet des conseils
            self.recup_onglet_conseils().changeCombobox = True

            # Récupérer les élèves de l'onglet des élèves et les mettre à jour dans l'onglet des conseils
            eleves = self.recup_onglet_eleves().recup_eleves()
            onglet_conseils = self.recup_onglet_conseils()
            onglet_conseils.eleves1.clear()
            onglet_conseils.eleves1.addItems(eleves)
            onglet_conseils.eleves2.clear()
            onglet_conseils.eleves2.addItems(eleves)

            # Désactiver le changement de la combobox dans l'onglet des conseils
            self.recup_onglet_conseils().changeCombobox = False

    # Méthode pour changer la langue de l'interface
    def changer_langue(self):
        # Modifier les textes des onglets avec les traductions
        self.onglets_widget.setTabText(0, self.langues.tr('eleves'))
        self.onglets_widget.setTabText(1, self.langues.tr('conseils'))


if __name__ == '__main__':
    # Création de l'application
    programme = QApplication(sys.argv)

    # Création de la fenêtre principale
    fenetre = DelegueMaster()

    # Application du thème à l'application
    qt_material.apply_stylesheet(programme, theme=fenetre.themes.convertir_theme_int_en_str())

    # Affichage de la fenêtre principale et démarrage de l'application
    fenetre.show()
    sys.exit(programme.exec())
