import sqlite3

from PyQt6.QtWidgets import QWidget, QVBoxLayout, QTabWidget, QLabel, QTextEdit, QDoubleSpinBox, QComboBox, \
    QMessageBox


class OngletConseils(QWidget):
    def __init__(self, delegue_master):
        super().__init__()

        # Initialisation de la classe avec le délégué maître
        self.delegue_master = delegue_master
        self.changement_combo_box = True  # Un indicateur pour suivre les changements dans les boîtes de combinaison

        # Création du layout principal
        layout = QVBoxLayout()

        # Récupération de la traduction pour "appreciations"
        tr = self.delegue_master.langues.tr
        appreciations = tr('appreciations')

        # Création des onglets pour les semestres
        self.onglets_semestre = QTabWidget()
        layout.addWidget(self.onglets_semestre)

        # Première onglet pour le premier semestre
        self.premier_semestre_tab = QWidget()
        self.premier_layout = QVBoxLayout()
        self.premier_semestre_tab.setLayout(self.premier_layout)

        # Étiquette pour les élèves du premier semestre
        OngletConseils.label_eleves1 = QLabel(tr('eleves'))
        self.premier_layout.addWidget(OngletConseils.label_eleves1)

        # Menu déroulant pour les élèves du premier semestre
        OngletConseils.eleves1 = QComboBox()
        self.premier_layout.addWidget(OngletConseils.eleves1)

        # Étiquette pour la remarque du premier semestre
        OngletConseils.label_remarque1 = QLabel(tr('remarque'))
        self.premier_layout.addWidget(OngletConseils.label_remarque1)

        # Zone de texte pour la remarque du premier semestre
        self.remarque1 = QTextEdit()
        self.remarque1.textChanged.connect(lambda: self.enregistrement_db())
        self.premier_layout.addWidget(self.remarque1)

        # Étiquette pour la moyenne du premier semestre
        OngletConseils.label_moyenne1 = QLabel(tr('moyenne'))
        self.premier_layout.addWidget(OngletConseils.label_moyenne1)

        # Boîte à double spin pour la moyenne du premier semestre
        self.moyenne1 = QDoubleSpinBox()
        self.moyenne1.setRange(0, self.delegue_master.profile.note_max)
        self.moyenne1.valueChanged.connect(lambda: self.enregistrement_db())
        self.premier_layout.addWidget(self.moyenne1)

        # Étiquette pour l'appréciation du premier semestre
        OngletConseils.label_appreciation1 = QLabel(tr('appreciation'))
        self.premier_layout.addWidget(OngletConseils.label_appreciation1)

        # Menu déroulant pour l'appréciation du premier semestre
        self.appreciation1 = QComboBox()
        self.appreciation1.currentIndexChanged.connect(lambda: self.enregistrement_db())
        self.appreciation1.addItems(appreciations)
        self.premier_layout.addWidget(self.appreciation1)

        # Connexions pour mettre à jour les remarques, moyennes et appréciations
        OngletConseils.eleves1.currentIndexChanged.connect(lambda index: self.mise_a_jour_remarques(0))
        OngletConseils.eleves1.currentIndexChanged.connect(lambda index: self.mise_a_jour_moyenne(0))
        OngletConseils.eleves1.currentIndexChanged.connect(lambda index: self.mise_a_jour_mentions(0))

        # Ajout du premier onglet au widget d'onglets
        self.onglets_semestre.addTab(self.premier_semestre_tab, tr('semestre_1'))

        # Deuxième onglet pour le deuxième semestre
        self.deuxieme_semestre_tab = QWidget()
        self.deuxieme_layout = QVBoxLayout()
        self.deuxieme_semestre_tab.setLayout(self.deuxieme_layout)

        # Étiquette pour les élèves du deuxième semestre
        OngletConseils.label_eleves2 = QLabel(tr('eleves'))
        self.deuxieme_layout.addWidget(OngletConseils.label_eleves2)

        # Menu déroulant pour les élèves du deuxième semestre
        OngletConseils.eleves2 = QComboBox()
        self.deuxieme_layout.addWidget(OngletConseils.eleves2)

        # Étiquette pour la remarque du deuxième semestre
        OngletConseils.label_remarque2 = QLabel(tr('remarque'))
        self.deuxieme_layout.addWidget(OngletConseils.label_remarque2)

        # Zone de texte pour la remarque du deuxième semestre
        self.remarque2 = QTextEdit()
        self.remarque2.textChanged.connect(lambda: self.enregistrement_db())
        self.deuxieme_layout.addWidget(self.remarque2)

        # Étiquette pour la moyenne du deuxième semestre
        OngletConseils.label_moyenne2 = QLabel(tr('moyenne'))
        self.deuxieme_layout.addWidget(OngletConseils.label_moyenne2)

        # Boîte à double spin pour la moyenne du deuxième semestre
        self.moyenne2 = QDoubleSpinBox()
        self.moyenne2.setRange(0, self.delegue_master.profile.note_max)
        self.moyenne2.valueChanged.connect(lambda: self.enregistrement_db())
        self.deuxieme_layout.addWidget(self.moyenne2)

        # Étiquette pour l'appréciation du deuxième semestre
        OngletConseils.label_appreciation2 = QLabel(tr('appreciation'))
        self.deuxieme_layout.addWidget(OngletConseils.label_appreciation2)

        # Menu déroulant pour l'appréciation du deuxième semestre
        self.appreciation2 = QComboBox()
        self.appreciation2.currentIndexChanged.connect(lambda: self.enregistrement_db())
        self.appreciation2.addItems(appreciations)
        self.deuxieme_layout.addWidget(self.appreciation2)

        # Connexions pour mettre à jour les remarques, moyennes et appréciations
        OngletConseils.eleves2.currentIndexChanged.connect(lambda index: self.mise_a_jour_remarques(1))
        OngletConseils.eleves2.currentIndexChanged.connect(lambda index: self.mise_a_jour_moyenne(1))
        OngletConseils.eleves2.currentIndexChanged.connect(lambda index: self.mise_a_jour_mentions(1))

        # Ajout des onglets semestre au widget d'onglets "conseils"
        self.onglets_semestre.addTab(self.premier_semestre_tab, tr('semestre_1'))
        self.onglets_semestre.addTab(self.deuxieme_semestre_tab, tr('semestre_2'))

        # Indicateur de changement de combobox initialisé à False
        self.changement_combo_box = False

        # Configuration du layout principal
        self.setLayout(layout)

        # Méthode pour mettre à jour les remarques en fonction de l'onglet et de l'index

    def mise_a_jour_remarques(self, idTab):
        self.changement_combo_box = True
        remarques = self.recup_db(idTab, 'remarques')
        if idTab == 0:
            self.remarque1.setPlainText(remarques)
        elif idTab == 1:
            self.remarque2.setPlainText(remarques)
        self.changement_combo_box = False

        # Méthode pour mettre à jour les moyennes en fonction de l'onglet et de l'index

    def mise_a_jour_moyenne(self, idTab):
        self.changement_combo_box = True
        moyennes = self.recup_db(idTab, 'moyennes')
        if idTab == 0:
            if moyennes:
                self.moyenne1.setValue(moyennes)
            else:
                self.moyenne1.setValue(0)
        elif idTab == 1:
            if moyennes:
                self.moyenne2.setValue(moyennes)
            else:
                self.moyenne2.setValue(0)
        self.changement_combo_box = False

        # Méthode pour mettre à jour les appréciations en fonction de l'onglet et de l'index

    def mise_a_jour_mentions(self, idTab):
        self.changement_combo_box = True
        mention = self.recup_db(idTab, 'mentions')
        if idTab == 0:
            if mention:
                self.appreciation1.setCurrentIndex(mention)
            else:
                self.appreciation1.setCurrentIndex(0)
        elif idTab == 1:
            if mention:
                self.appreciation2.setCurrentIndex(mention)
            else:
                self.appreciation2.setCurrentIndex(0)
        self.changement_combo_box = False

        # Méthode pour enregistrer les données dans la base de données

    def enregistrement_db(self):
        if self.delegue_master.db_temp_file and not self.changement_combo_box:
            try:
                conn = sqlite3.connect(self.delegue_master.db_temp_file)
                cursor = conn.cursor()
                tab = self.onglets_semestre.currentIndex()

                if tab == 0:
                    idEleve = OngletConseils.eleves1.currentIndex()
                    if idEleve != -1:
                        remarque = self.remarque1.toPlainText()
                        moyenne = self.moyenne1.value()
                        appreciation = self.appreciation1.currentIndex()
                        cursor.execute('UPDATE semestre1 SET remarques = ? WHERE idEleves = ?', (remarque, idEleve + 1))
                        cursor.execute('UPDATE semestre1 SET moyennes = ? WHERE idEleves = ?', (moyenne, idEleve + 1))
                        cursor.execute('UPDATE semestre1 SET mentions = ? WHERE idEleves = ?',
                                       (appreciation, idEleve + 1))

                elif tab == 1:
                    idEleve = OngletConseils.eleves2.currentIndex()
                    if idEleve != -1:
                        remarque = self.remarque2.toPlainText()
                        moyenne = self.moyenne2.value()
                        appreciation = self.appreciation2.currentIndex()
                        cursor.execute('UPDATE semestre2 SET remarques = ? WHERE idEleves = ?', (remarque, idEleve + 1))
                        cursor.execute('UPDATE semestre2 SET moyennes = ? WHERE idEleves = ?', (moyenne, idEleve + 1))
                        cursor.execute('UPDATE semestre2 SET mentions = ? WHERE idEleves = ?',
                                       (appreciation, idEleve + 1))

                conn.commit()
                conn.close()

            except Exception as e:
                QMessageBox.critical(self, self.delegue_master.langues.tr('erreur'),
                                     self.delegue_master.langues.tr('message_erreur') + str(e))

        # Méthode pour récupérer des données à partir de la base de données

    def recup_db(self, idTab, table):
        try:
            conn = sqlite3.connect(self.delegue_master.db_temp_file)
            cursor = conn.cursor()

            if idTab == 0:
                cursor.execute(f'select {table} FROM semestre1 WHERE idEleves = ?',
                               (OngletConseils.eleves1.currentIndex() + 1,))
            elif idTab == 1:
                cursor.execute(f'select {table} FROM semestre2 WHERE idEleves = ?',
                               (OngletConseils.eleves2.currentIndex() + 1,))

            result = cursor.fetchone()

            if result:
                return result[0]

            conn.close()

        except Exception as e:
            QMessageBox.critical(self, self.delegue_master.langues.tr('erreur'),
                                 self.delegue_master.langues.tr('message_erreur') + str(e))

        # Méthode pour changer la langue des éléments d'interface

    def changer_langue(self):
        tr = self.delegue_master.langues.tr

        OngletConseils.label_eleves1.setText(tr('eleves'))
        OngletConseils.label_remarque1.setText(tr('remarque'))
        OngletConseils.label_moyenne1.setText(tr('moyenne'))
        OngletConseils.label_appreciation1.setText(tr('appreciation'))

        OngletConseils.label_eleves2.setText(tr('eleves'))
        OngletConseils.label_remarque2.setText(tr('remarque'))
        OngletConseils.label_moyenne2.setText(tr('moyenne'))
        OngletConseils.label_appreciation2.setText(tr('appreciation'))

        ancien_index_appreciation1 = self.appreciation1.currentIndex()
        ancien_index_appreciation2 = self.appreciation2.currentIndex()

        self.appreciation1.clear()
        self.appreciation2.clear()

        self.appreciation1.addItems(tr('appreciations'))
        self.appreciation2.addItems(tr('appreciations'))

        self.appreciation1.setCurrentIndex(ancien_index_appreciation1)
        self.appreciation2.setCurrentIndex(ancien_index_appreciation2)

        self.onglets_semestre.setTabText(0, tr('semestre_1'))
        self.onglets_semestre.setTabText(1, tr('semestre_2'))

        # Méthode pour changer la note maximale dans la boîte à double spin

    def changer_note_maximum(self):
        self.moyenne1.setMaximum(self.delegue_master.profile.note_max)
        self.moyenne2.setMaximum(self.delegue_master.profile.note_max)