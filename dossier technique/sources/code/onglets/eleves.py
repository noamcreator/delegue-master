import sqlite3

from PyQt6.QtWidgets import QPushButton, QDialog, QMessageBox, QLabel, QHBoxLayout
from PyQt6.QtWidgets import QWidget, QVBoxLayout, QTableWidget, QLineEdit, QTableWidgetItem


class OngletEleves(QWidget):
    def __init__(self, delegue_master):
        super().__init__()

        self.delegue_master = delegue_master

        tr = self.delegue_master.langues.tr

        layout = QVBoxLayout()

        # Ajout du champ de recherche
        self.champ_de_recherche = QLineEdit()
        self.champ_de_recherche.setPlaceholderText(tr('rechercher'))
        self.champ_de_recherche.setClearButtonEnabled(True)
        self.champ_de_recherche.textChanged.connect(self.filter_table_recherche)
        layout.addWidget(self.champ_de_recherche)

        OngletEleves.table_eleves = QTableWidget()
        OngletEleves.table_eleves.setColumnCount(2)
        OngletEleves.table_eleves.setHorizontalHeaderLabels([tr('nom'), tr('prenom')])
        OngletEleves.table_eleves.currentItemChanged.connect(self.item_selectionne)
        OngletEleves.table_eleves.itemChanged.connect(self.renommer_nom_prenom_db)
        layout.addWidget(OngletEleves.table_eleves)

        # Ajout des boutons Ajouter et Supprimer
        toolbar = QHBoxLayout()
        OngletEleves.ajouter_eleve_bouton = QPushButton(tr('ajouter_eleve'))
        OngletEleves.ajouter_eleve_bouton.clicked.connect(self.ajouter_eleve_dialog)
        toolbar.addWidget(OngletEleves.ajouter_eleve_bouton)

        self.supprimer_eleve_bouton = QPushButton(tr('supprimer_eleve'))
        self.supprimer_eleve_bouton.clicked.connect(self.supprimer_eleve)
        self.supprimer_eleve_bouton.setEnabled(False)
        toolbar.addWidget(self.supprimer_eleve_bouton)
        layout.addLayout(toolbar)

        self.setLayout(layout)

    def item_selectionne(self):
        # Vérifie si une ligne est sélectionnée dans la table des élèves
        if OngletEleves.table_eleves.currentRow() != -1:
            # Activer les options de suppression d'élève si une ligne est sélectionnée
            self.delegue_master.menuBar.supprimer_eleve.setEnabled(True)
            self.supprimer_eleve_bouton.setEnabled(True)
        else:
            # Désactiver les options de suppression si aucune ligne n'est sélectionnée
            self.delegue_master.menuBar.supprimer_eleve.setEnabled(False)
            self.supprimer_eleve_bouton.setEnabled(False)

    def renommer_nom_prenom_db(self):
        # Récupère la ligne actuellement sélectionnée
        current_row = OngletEleves.table_eleves.currentRow()
        # Vérifie s'il y a une ligne sélectionnée
        if current_row == -1:
            return  # Quitte la fonction s'il n'y a pas de ligne sélectionnée

        # Récupère le nom et le prénom de l'élève dans la table
        nom = OngletEleves.table_eleves.item(current_row, 0).text()
        prenom = OngletEleves.table_eleves.item(current_row, 1).text()
        db = self.delegue_master.db_temp_file
        conn = sqlite3.connect(db)
        c = conn.cursor()

        # Met à jour les données dans la base de données avec le nouveau nom et prénom
        c.execute("UPDATE eleves SET nom = ?, prenom = ? WHERE idEleves = ?", (nom, prenom, current_row + 1))

        conn.commit()
        conn.close()

    def filter_table_recherche(self):
        # Récupère le texte de recherche en minuscules
        search_text = self.champ_de_recherche.text().lower()

        # Parcourt les lignes de la table des élèves
        for row in range(OngletEleves.table_eleves.rowCount()):
            # Récupère le nom et le prénom de chaque élève dans la table en minuscules
            nom = OngletEleves.table_eleves.item(row, 0).text().lower()
            prenom = OngletEleves.table_eleves.item(row, 1).text().lower()

            # Cache ou montre la ligne en fonction du texte de recherche
            if search_text in nom or search_text in prenom:
                OngletEleves.table_eleves.setRowHidden(row, False)
            else:
                OngletEleves.table_eleves.setRowHidden(row, True)

    def ajouter_eleve_dialog(self):
        OngletEleves.table_eleves.clearSelection()
        dialog = QDialog(self)

        tr = self.delegue_master.langues.tr

        dialog.setWindowTitle(tr('ajouter_eleve'))

        layout = QVBoxLayout()

        label_nom = QLabel(tr("nom")+":")
        edit_nom = QLineEdit()
        layout.addWidget(label_nom)
        layout.addWidget(edit_nom)

        label_prenom = QLabel(tr('prenom') +":")
        edit_prenom = QLineEdit()
        layout.addWidget(label_prenom)
        layout.addWidget(edit_prenom)

        button_ok = QPushButton(tr('ok'))
        button_ok.clicked.connect(dialog.accept)
        layout.addWidget(button_ok)

        dialog.setLayout(layout)

        if dialog.exec() == QDialog.DialogCode.Accepted:
            # enlever la selection d'une table
            nom = edit_nom.text()
            nom = nom.upper()  # mettre le nom en majuscule

            prenom = edit_prenom.text()
            # mettre la premier lettre de chaque mot en majuscule
            prenom = ' '.join([word.capitalize() for word in prenom.split()])
            self.ajouter_eleve(nom, prenom)

    def supprimer_eleve(self):
        selected_row = OngletEleves.table_eleves.currentRow()

        if selected_row != -1:
            nom = OngletEleves.table_eleves.item(selected_row, 0).text()
            prenom = OngletEleves.table_eleves.item(selected_row, 1).text()

            confirmation = QMessageBox.question(self.delegue_master, "Confirmation",
                                                f'Êtes-vous sûr de vouloir supprimer l\'élève {nom} {prenom} ?',
                                                QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
            if confirmation == QMessageBox.StandardButton.Yes:
                # Supprimer l'élève de la base de données
                db = self.delegue_master.db_temp_file
                conn = sqlite3.connect(db)
                c = conn.cursor()
                c.execute("DELETE FROM eleves WHERE nom = ? AND prenom = ?", (nom, prenom))
                c.execute("DELETE FROM semestre1 WHERE idEleves = ?", (selected_row + 1,))
                c.execute("DELETE FROM semestre2 WHERE idEleves = ?", (selected_row + 1,))

                # Réindexer les IDs dans la table eleves
                c.execute("UPDATE eleves SET idEleves = idEleves - 1 WHERE idEleves > ?", (selected_row + 1,))

                # Réindexer les IDs dans les tables semestre1 et semestre2
                c.execute("UPDATE semestre1 SET idEleves = idEleves - 1 WHERE idEleves > ?", (selected_row + 1,))
                c.execute("UPDATE semestre2 SET idEleves = idEleves - 1 WHERE idEleves > ?", (selected_row + 1,))

                conn.commit()
                conn.close()

                # Supprimer la ligne correspondante dans le QTableWidget
                OngletEleves.table_eleves.removeRow(selected_row)
                OngletEleves.table_eleves.clearSelection()
            else:
                pass
        else:
            QMessageBox.warning(self.delegue_master, "Aucune sélection", "Veuillez sélectionner un élève à supprimer.",
                                QMessageBox.StandardButton.Ok)

    def recup_table(self):
        return OngletEleves.table_eleves

    def effacer_table(self):
        OngletEleves.table_eleves.setRowCount(0)

        # Mettre à jour le fichier temps et la base de données via la table
        db = self.delegue_master.db_temp_file
        conn = sqlite3.connect(db)
        c = conn.cursor()

        c.execute("DELETE FROM eleves")
        c.execute("DELETE FROM semestre1")
        c.execute("DELETE FROM semestre2")

        conn.commit()
        conn.close()

    def mettre_eleve_indice(self, index, nom, prenom):
        OngletEleves.table_eleves.setItem(index, 0, QTableWidgetItem(nom))
        OngletEleves.table_eleves.setItem(index, 1, QTableWidgetItem(prenom))

    def ajouter_eleve(self, nom, prenom):
        OngletEleves.table_eleves.setRowCount(OngletEleves.table_eleves.rowCount() + 1)
        self.mettre_eleve_indice(OngletEleves.table_eleves.rowCount() - 1, nom, prenom)

        # Mettre a jour le fichier temps et la base de données via la table
        db = self.delegue_master.db_temp_file
        conn = sqlite3.connect(db)
        c = conn.cursor()

        c.execute("INSERT INTO eleves (nom, prenom) VALUES (?, ?)", (nom, prenom))
        conn.commit()

        c.execute("SELECT ifnull(max(idEleves),0) FROM ELEVES")
        idEleve = c.fetchone()

        c.execute("INSERT INTO semestre1 (idEleves) VALUES (?)", idEleve)
        c.execute("INSERT INTO semestre2 (idEleves) VALUES (?)", idEleve)

        conn.commit()
        conn.close()

    def recup_eleves(self):
        return [OngletEleves.table_eleves.item(i, 0).text() + " " + OngletEleves.table_eleves.item(i, 1).text() for i in
                range(OngletEleves.table_eleves.rowCount())]

    def changer_langue(self):
        # Change la langue des éléments d'interface utilisateur en fonction de la langue sélectionnée
        tr = self.delegue_master.langues.tr
        self.champ_de_recherche.setPlaceholderText(tr('rechercher'))

        OngletEleves.table_eleves.setHorizontalHeaderLabels([tr('nom'), tr('prenom')])

        OngletEleves.ajouter_eleve_bouton.setText(tr('ajouter_eleve'))
        self.supprimer_eleve_bouton.setText(tr('supprimer_eleve'))
