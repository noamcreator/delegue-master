
Afin de lancer le programme, il faut : 
- S'assurer que la base de donnée modèle et le logo sont téléchargés
- Lancer le fichier python principal (DelegueMaster.py) et veiller a ce que les chemins d'accès dans les "imports" correspondent bien a ceux téléchargés
- S'assurer que la bibliothèque PyQt6 est installé, cette dernière peut être installer dans l'invite de commande avec : " python -m pip install PyQt6 " 
- S'assurer que la bibliothèque reportlab est installé, cette dernière peut être installer dans l'invite de commande avec : " python -m pip install reportlab "
- S'assurer que qt_material est installé, cela peut être fait dans l'invite de commande avec : " python -m pip install qt_material "